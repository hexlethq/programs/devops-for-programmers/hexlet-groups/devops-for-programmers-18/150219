resource "digitalocean_certificate" "cert" {
  name    = "certificate"
  type    = "lets_encrypt"
  domains = [digitalocean_domain.domain.name]
}
