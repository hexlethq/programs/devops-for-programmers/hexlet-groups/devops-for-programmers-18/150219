# Vagrant

Создадим изолированное окружение для разработки с помощью Vagrant и запустим внутри него [Fastify](https://github.com/fastify/fastify). Fastify – микрофреймворк на JavaScript, который позволяет создать простой сайт используя буквально 20 строк кода (скопированных из документации).

## Ссылки

* [Установка Node.js](https://github.com/nodesource/distributions#installation-instructions)
* [Vagrant: Проброс портов](https://www.vagrantup.com/docs/networking/forwarded_ports)

## Задачи

1. Установите [VirtualBox](https://www.virtualbox.org/wiki/Linux_Downloads)
2. Установите [Vagrant](https://www.vagrantup.com/docs/installation)
3. Инициализируйте Vagrant-проект

    ```sh
    vagrant init
    # image ubuntu/focal64.
    ```

4. Выставьте наружу 3000 порт. На нем запустится Fastify.
5. Установите Node.JS (JavaScript Runtime) с помощью [shell-скрипта](https://www.vagrantup.com/docs/provisioning/shell).
6. Инициализируйте Fastify-проект и убедитесь что он работает

    ```sh
    vagrant ssh
    cd /vagrant
    npm init -y fastify
    npm install
    FASTIFY_ADDRESS=0.0.0.0 npm run dev # проверяем что все работает
    # open localhost:3000
    ```

# Подсказки

* Убедитесь, что установлена последняя версия npm (7+)
* В VirtualBox по соображениям безопасности отключена возможность создания символических ссылок в общедоступных каталогах, а каталог */vagrant* внутри виртуальной машины как раз таким и является. Чтобы корректно отработала команда `npm install` нужно включить создание симлинков. В этом вам поможет статья — [VirtualBox 6: How to enable symlinks for shared folders](https://www.speich.net/articles/en/2018/12/24/virtualbox-6-how-to-enable-symlinks-in-a-linux-guest-os/)
