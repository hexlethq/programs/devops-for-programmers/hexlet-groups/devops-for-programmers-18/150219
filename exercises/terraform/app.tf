resource "digitalocean_droplet" "app1" {
    image = "docker-20-04"
    name = "web-terraform-homework-01"
    region = "ams3"
    size = "s-1vcpu-1gb"
    ssh_keys = [var.ssh_key]
}

resource "digitalocean_droplet" "app2" {
    image = "docker-20-04"
    name = "web-terraform-homework-02"
    region = "ams3"
    size = "s-1vcpu-1gb"
    ssh_keys = [var.ssh_key]
}

resource "digitalocean_loadbalancer" "public" {
  name   = "loadbalancer-1"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 22
    protocol = "tcp"
  }

  droplet_ids = [digitalocean_droplet.app1.id,digitalocean_droplet.app2.id,]
}

/*
resource "digitalocean_domain" "default" {
  name       = "genusor.xyz"
  ip_address = digitalocean_droplet.public.ipv4_address
}
*/

output "instance_ip_addr" {
  value = [digitalocean_droplet.app1.ipv4_address,digitalocean_droplet.app2.ipv4_address]
}
