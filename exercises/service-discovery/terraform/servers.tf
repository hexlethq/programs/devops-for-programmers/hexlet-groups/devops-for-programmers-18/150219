resource "digitalocean_droplet" "apps" {
  count              = 2
  image              = "docker-20-04"
  region             = "ams3"
  name               = "service-discovery-${count.index}"
  size               = "s-1vcpu-1gb"
  private_networking = true

  ssh_keys = [digitalocean_ssh_key.discovery.id,var.ssh_key]
}
