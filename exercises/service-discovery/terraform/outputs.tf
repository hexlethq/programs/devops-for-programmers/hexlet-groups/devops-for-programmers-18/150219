output "webserver_ips" {
  value = digitalocean_droplet.apps.*.ipv4_address
}
