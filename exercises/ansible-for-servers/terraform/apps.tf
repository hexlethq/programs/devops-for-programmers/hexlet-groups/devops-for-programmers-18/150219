resource "digitalocean_droplet" "apps" {
    count = 2
    image = "ubuntu-20-10-x64"
    name = "ansible-for-servers-homework-${count.index}"
    region = "ams3"
    size = "s-1vcpu-1gb"
    ssh_keys = [var.ssh_key]
}

resource "digitalocean_loadbalancer" "loadbalancer" {
  name   = "ansible-for-servers-loadbalancer"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 22
    protocol = "tcp"
  }

  droplet_ids = digitalocean_droplet.apps.*.id
}

resource "digitalocean_domain" "loadbalancer" {
  name       = "genusor.xyz"
  ip_address = digitalocean_loadbalancer.loadbalancer.ip
}


output "instance_ip_addr" {
  value = digitalocean_droplet.apps.*
}

output "public_ssh_key" {
  value = var.ssh_key
}
